# Backup Outline Instance

Creates, downloads (and deletes old) exports of outline instances.

## Contents
* [Getting Started](#getting-started)
*    [Prerequisites](#prerequisites)
*    [Installing](#installing)
* [Running the tests](#running-the-tests)
* [Deployment](#deployment)
* [Built With](#built-with)
* [Contributing](#contributing)
*    [Roadmap](#roadmap)
* [Versioning](#versioning)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)
*    [Project History](#project-history)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

I recommend you to use the `setup_project.sh` script by running

```
./setup_project.sh
```

### Installation
1. Clone the repository
`git clone git@gitlab.com:MaxValue/outline-backup.git`
2. Fill in the config file
`cp config.yml.tmpl config.yml`
`vim config.yml`
3. Test the backup
`./backup_outline.sh`
4. Fill in the crontab
`0 * * * * /path/to/backup_outline.sh /path/to/backup/location`

## Built With

* [Sublime Text 3](https://www.sublimetext.com/) - The code editor I use
* [Python 3](https://www.sublimetext.com/) - For running the dev web server

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Max Fuxjäger** - *Initial work* - [MaxValue](https://gitlab.com/maxvalue/)

See also the list of [contributors](https://gitlab.com/MaxValue/outline-backup/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

### Project History
This project was created because I (Max) needed a simple script to create backups of an outline instance.