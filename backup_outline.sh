#!/usr/bin/env bash

installdir=$(dirname "${BASH_SOURCE[0]}")
source "$installdir/venv/bin/activate"
  "$installdir/backup_outline.py" --config "$installdir/config.yml" --interval 5 --deleteold --log "$installdir/log.txt" "$1"
  backupsuccess=$?
deactivate
exit $backupsuccess
