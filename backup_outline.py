#!/usr/bin/env python3
# coding: utf-8

import argparse
import csv
import datetime
import jmespath
import json
import logging
import os
import requests
import sys
import time
import yaml

def request(*args, **kwargs):
    response = requests.request(
        *args,
        **kwargs,
    )
    if response.status_code == 200:
        return response
    else:
        exit(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(argument_default=False, description="Create an outline backup.")
    parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
    parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
    parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
    parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
    parser.add_argument("--config", type=argparse.FileType("r", encoding="utf-8"), default="config.yml", help="The config file to use.")
    parser.add_argument("--interval", default="2", help="The seconds to wait between download retries.")
    parser.add_argument("--deleteold", action="store_true", help="Whether to delete old exports from the Outline instance.")
    parser.add_argument("--nocreateoutpath", action="store_true", help="Whether not to create the outpath if it does not exist.")
    parser.add_argument("outpath", nargs="?", default=".", help="The directory path in which to save the export data file.")
    args = parser.parse_args()

    if args.quiet:
        loglevel = 100
    elif args.debug:
        loglevel = logging.DEBUG
    elif args.verbose:
        loglevel = logging.INFO
    else:
        loglevel = logging.WARNING

    if args.log:
        logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
    else:
        logging.basicConfig(level=loglevel)

    config = yaml.safe_load(args.config)

    outputLines = 0

    response = request(
        "post",
        f"https://{config['domain']}/api/collections.export_all",
        headers={
            "Authorization": f"Bearer {config['token']}"
        },        
    )
    backupDocumentID = jmespath.search("data.fileOperation.id", response.json())
    downloadReady = False
    while not downloadReady:
        response = request(
            "post",
            f"https://{config['domain']}/api/fileOperations.info",
            headers={
                "Authorization": f"Bearer {config['token']}"
            },
            data={
                "id": backupDocumentID
            },
        )
        exportMetadata = response.json()
        if jmespath.search("data.state", exportMetadata) == "complete":
            downloadReady = True
            break
        time.sleep(int(args.interval))
    response = request(
        "post",
        f"https://{config['domain']}/api/fileOperations.redirect",
        stream=True,
        headers={
            "Authorization": f"Bearer {config['token']}"
        },
        data={
            "id": backupDocumentID
        },
    )
    if args.nocreateoutpath:
        if not os.path.exists(args.outpath):
            exit(1)
    else:
        os.makedirs(args.outpath, exist_ok=True)
    dateExported = datetime.datetime.strptime(jmespath.search("data.createdAt", exportMetadata), "%Y-%m-%dT%H:%M:%S.%f%z")
    outFile_path = os.path.join(args.outpath, f"outlinebackup_{config['domain']}_{dateExported.strftime('%Y-%m-%dT%H%M%S.%f')}.zip")
    with open(outFile_path, 'wb') as outputFile:
        for chunk in response.iter_content(chunk_size=128):
            outputFile.write(chunk)
    if args.deleteold:
        response = request(
            "post",
            f"https://{config['domain']}/api/fileOperations.list",
            headers={
                "Authorization": f"Bearer {config['token']}"
            },
            data={
                "limit": 100,
                "offset": 0,
                "type": "export",
            },
        )
        oldExports = jmespath.search("data[].id", response.json())
        for oldExport in oldExports:
            response = request(
                "post",
                f"https://{config['domain']}/api/fileOperations.delete",
                headers={
                    "Authorization": f"Bearer {config['token']}"
                },
                data={
                    "id": oldExport
                },                
            )
